defmodule BlogApp.Schema.Types do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: BlogApp.Repo

  object :accounts_user do
    field :id, :id
    field :name, :string
    field :email, :string
    #
    # Take note on the names here:
    # list_of(:blog_post) -> :blog_post maps to the Absinthe object down below
    # while assoc(:blog_posts) maps to the table!
    # The table names are named that because of Phoenix contexts that we made earlier!
    #
    field :posts, list_of(:blog_post), resolve: assoc(:blog_posts)
  end

  object :blog_post do
    field :id, :id
    field :title, :string
    field :body, :string
    #
    # You can see the same pattern here:
    # field :user, :accounts_user -> :accounts_user = the object above
    # assoc(:accounts_users) -> :accounts_users = the accounts_users table
    #
    field :user, :accounts_user, resolve: assoc(:accounts_users)
  end
end